<?php

namespace App\Http\Controllers;

use App\Fees;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;

class FeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $fees=Fees::all();
        return view('101833.fees')->with('fees',$fees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('101833.fees');

    }
    public function search()
    {
        //dd('h');
        $fees=Fees::where('admno',Input::get('admno'))->get();
        // dd($fees);
        return view('101833.fees')->with('fees',$fees);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,['name'=>'required',
            'dob'=>'required',
            'studentno'=>'required',
            'amount'=>'required']);
            $fees=new Fees();

            $fees->admno=$request->input('studentno');
            $fees->name = $request->input('name');
            $fees->amount=$request->input('amount');
            $fees->dob=$request->input('dob');
            
            
            $fees->save();
             return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function show(Fees $fees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function edit(Fees $fees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fees $fees)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fees $fees)
    {
        //
    }
}
