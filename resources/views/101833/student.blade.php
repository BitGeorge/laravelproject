<a href="{{url('/student')}}">Student</a>
<a href="{{url('/fees')}}">Fees</a>
<br>
<h1>Student</h1>

<link rel="stylesheet" href="{{asset('css/app.css')}}" />


{{ Form::open(['action'=>'StudentController@store','method'=>'POST']) }}
<div class="form-group ">
	{!! Form::label('text', 'Student Name') !!}
	{{ Form::text('name', '', ['class' => 'form-control','placeholder' => 'Enter student name here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Date of Birth') !!}
	{{ Form::Date('dob', '', ['class' => 'form-control','placeholder' => 'Enter dob here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Student number') !!}
	{{ Form::number('studentno', '', ['class' => 'form-control','placeholder' => 'Enter Student number here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Address') !!}
	{{ Form::text('address', '', ['class' => 'form-control','placeholder' => 'Enter address here']) }}
</div>
{!! Form::submit('Submit',['class' => 'btn btn-primary']) !!}

{{ Form::close() }}
<br>
<hr>
<br>

<table class="table table-bordered">
	<tr>
		<th>Name</th>
		<th>Admno</th>
		<th>Dob</th>
		<th>Address</th>
	</tr>
	@if(count($students)>0)
	@foreach ($students as $posts)


	<tr>
		<td>
			<h4>{{$posts->fullname}}</h4>
		</td>
		<td>
			<h4>{{$posts->admno}}</h4>
		</td>
		<td>
			<h4>{{$posts->dob}}</h4>
		</td>
		<td>
			<h4>{{$posts->address}}</h4>
		</td>
	</tr>




	@endforeach
	@else
	<p>No record found </p>
	@endif
</table>