<a href="{{url('/student')}}">Student</a>
<a href="{{url('/fees')}}">Fees</a>
<br>
<h1>Fees</h1>
<link rel="stylesheet" href="{{asset('css/app.css')}}" />

{{ Form::open(['action'=>'FeesController@store','method'=>'POST']) }}
<div class="form-group ">
	{!! Form::label('text', 'Student Name') !!}
	{{ Form::text('name', '', ['class' => 'form-control','placeholder' => 'Enter student name here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Date of Payment') !!}
	{{ Form::Date('dob', '', ['class' => 'form-control','placeholder' => 'Enter date of payment here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Student number') !!}
	{{ Form::number('studentno', '', ['class' => 'form-control','placeholder' => 'Enter Student number here']) }}
</div>
<div class="form-group ">
	{!! Form::label('text', 'Amount') !!}
	{{ Form::number('amount', '', ['class' => 'form-control','placeholder' => 'Enter amount here']) }}
</div>
{!! Form::submit('Submit',['class' => 'btn btn-primary']) !!}

{{ Form::close() }}

<br>
<hr>
<br>

<form action="{{url('search')}}" method="GET" role="search">
	{{ csrf_field() }}
	<div class="input-group">
		<input type="text" class="form-control" name="admno" placeholder="Search Admisson number"> <span
			class="input-group-btn">
			<button type="submit" class="btn btn-default">
				Submit
			</button>
		</span>
	</div>
</form>
<table class="table table-bordered">
	<tr>
		<th>Name</th>
		<th>Admno</th>
		<th>Dob</th>
		<th>amount</th>
	</tr>

	<?php $cout=0 ?>

	@if(count($fees)>0)
	@foreach ($fees as $posts)


	<tr>
		<td>
			<h4>{{$posts->name}}</h4>
		</td>
		<td>
			<h4>{{$posts->admno}}</h4>
		</td>
		<td>
			<h4>{{$posts->dob}}</h4>
		</td>
		<td>
			<h4>{{$posts->amount}}</h4>
		</td>
	</tr>
	{{$cout=$cout+$posts->amount}}





	@endforeach
	@else
	<p>No record found </p>
	@endif
	<tr>
		<th>Total Fees</th>
		<td></td>
		<td></td>
		<td><b>{{$cout}}</b></td>
	</tr>
</table>




<div class="container">
	@if(isset($details))
	<p> The Search results for your query <b> {{ $query }} </b> are :</p>
	<h2>Sample User details</h2>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Admno</th>
				<th>Dob</th>
				<th>amount</th>
			</tr>
		</thead>
		<tbody>
			@foreach($details as $user)
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->admno}}</td>
				<td>{{$user->dob}}</td>
				<td>{{$user->amount}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
</div>